import os
if not 'figsSortingDIY' in os.listdir("."):
    os.mkdir('figsSortingDIY')
if not 'code' in os.listdir("."):
    os.mkdir('code')

import urllib.request as url  #import the module
addr = 'https://gitlab.com/c_pouzat/spike-sorting-the-diy-way/-/raw/master/org/code/sorting_with_python.py'
url.urlretrieve(addr,'sorting_with_python.py')

import sorting_with_python as swp

import numpy as np
import matplotlib.pylab as plt

addr = 'https://gitlab.com/c_pouzat/spike-sorting-the-diy-way/-/raw/master/org/data/Locust4demo.h5'
url.urlretrieve(addr,'Locust4demo.h5')

# Import h5py module
import h5py
# Open file Locust4demo.h5
locust = h5py.File('Locust4demo.h5','r')
# Glimps at content
locust.visititems(print)

# Create a list with dataset names
dset_names = ['spont/site_' + str(i) for i in range(1,5)]
# Load the data in a list of numpy arrays
data = [locust[n][...] for n in dset_names]
# Close data file
locust.close()

from scipy.stats.mstats import mquantiles
np.set_printoptions(precision=3)
[mquantiles(x,prob=[0,0.25,0.5,0.75,1]) for x in data]

tt = np.arange(0,len(data[0]))/1.5e4
swp.plot_data_list(data,tt,0.1)

fname = 'figsSortingDIY/WholeRawData.png'
plt.savefig(fname)
fname

plt.xlim([0,0.2])

fname = 'figsSortingDIY/First200ms.png'
plt.savefig(fname)
plt.close()
fname

data = list(map(lambda x: (x-np.median(x))/swp.mad(x), data))

plt.plot(tt,data[0],color="black",lw=0.5)
plt.xlim([0,0.2])
plt.ylim([-17,13])
plt.axhline(y=1,color="red")
plt.axhline(y=-1,color="red")
plt.axhline(y=np.std(data[0]),color="blue",linestyle="dashed")
plt.axhline(y=-np.std(data[0]),color="blue",linestyle="dashed")
plt.xlabel('Time (s)')
plt.ylim([-20,15])

fname = 'figsSortingDIY/site1-with-MAD-and-SD.png'
plt.savefig(fname)
plt.close()
fname

dataQ = map(lambda x:
            mquantiles(x, prob=np.arange(0.01,0.99,0.001)),data)
dataQsd = map(lambda x:
              mquantiles(x/np.std(x), prob=np.arange(0.01,0.99,0.001)),
              data)
from scipy.stats import norm
qq = norm.ppf(np.arange(0.01,0.99,0.001))
plt.plot(np.linspace(-3,3,num=100),np.linspace(-3,3,num=100),
         color='grey')
colors = ['black', 'orange', 'blue', 'red']
for i,y in enumerate(dataQ):
    plt.plt.plot(qq,y,color=colors[i])

for i,y in enumerate(dataQsd):
    plt.plot(qq,y,color=colors[i],linestyle="dashed")

plt.xlabel('Normal quantiles')
plt.ylabel('Empirical quantiles')

fname = 'figsSortingDIY/check-MAD.png'
plt.savefig(fname)
plt.close()
fname

from scipy.signal import fftconvolve
from numpy import apply_along_axis as apply
data_filtered = apply(lambda x:
                      fftconvolve(x,np.array([1,1,1,1,1])/5.,'same'),
                      1,np.array(data))
dfiltered_mad_original = apply(swp.mad,1,data_filtered)
data_filtered = (data_filtered.T / \
                 dfiltered_mad_original).T
data_filtered_full = data_filtered.copy()
data_filtered[data_filtered > -4] = 0

plt.plot(tt, data[0],color='black',lw=0.5)
plt.axhline(y=-4,color="blue",linestyle="dashed")
plt.plot(tt, data_filtered[0,],color='red',lw=0.5)
plt.xlim([0,0.2])
plt.ylim([-20,15])
plt.xlabel('Time (s)')

fname = 'figsSortingDIY/compare-raw-and-filtered-data.png'
plt.savefig(fname)
plt.close()
fname

fig, axs = plt.subplots(nrows=5, sharex=True)
for i in range(4):
    ax = axs[i]
    ax.plot(tt, data_filtered_full[i],color='black',lw=0.5)
    ax.axhline(y=-4,color="blue",linestyle="dashed")
    ax.plot(tt, data_filtered[i],color='red',lw=0.5)

# sum across channels
data_sum = np.sum(data_filtered, axis=0)

ax = axs[4]
ax.plot(tt, data_sum, color='black')
ax.set_xlim([0.024,0.090])
ax.set_ylim([-100,10])
ax.set_xlabel('Time (s)')

fname = 'figsSortingDIY/sum-channel-filtered-data-all.png'
fig.savefig(fname)
plt.close()
fname

sp0 = swp.peak(-data_filtered.sum(0))

swp.plot_data_list_and_detection(data,tt,sp0)
plt.xlim([0,0.125])

fname = 'figsSortingDIY/check-spike-detection.png'
plt.savefig(fname)
plt.close()
fname

evts = swp.mk_events(sp0,np.array(data),49,50)
evts_median=apply(np.median,0,evts)
evts_mad=apply(swp.mad,0,evts)

plt.plot(evts_median, color='red', lw=2)
plt.axhline(y=0, color='black')
for i in np.arange(0,400,100): 
    plt.axvline(x=i, color='black', lw=2)

for i in np.arange(0,400,10): 
    plt.axvline(x=i, color='grey')

plt.plot(evts_median, color='red', lw=2)
plt.plot(evts_mad, color='blue', lw=2)

fname = 'figsSortingDIY/check-MAD-on-long-cuts.png'
plt.savefig(fname)
plt.close()
fname

evts = swp.mk_events(sp0,np.array(data),14,30)

fname = 'figsSortingDIY/first-200-of-evts.png'
plt.savefig(fname)
plt.close()
fname

def good_evts_fct(samp, thr=3):
    samp_med = apply(np.median,0,samp)
    samp_mad = apply(swp.mad,0,samp)
    below = samp_med < 0
    samp_r = samp.copy()
    for i in range(samp.shape[0]): samp_r[i,below] = 0
    samp_med[below] = 0
    res = apply(lambda x:
                np.all(abs(x-samp_med) < thr*samp_mad),
                1,samp_r)
    return res

goodEvts = good_evts_fct(evts,8)

swp.plot_events(evts[goodEvts,:][:200,:])

fname = 'figsSortingDIY/first-200-clean-of-evts.png'
plt.savefig(fname)
plt.close()
fname

from numpy.linalg import svd
varcovmat = np.cov(evts[goodEvts,:].T)
u, s, v = svd(varcovmat)

evt_idx = range(180)
evts_good_mean = np.mean(evts[goodEvts,:],0)
for i in range(4):
    plt.subplot(2,2,i+1)
    plt.plot(evt_idx,evts_good_mean, 'black',evt_idx,
             evts_good_mean + 5 * u[:,i],
             'red',evt_idx,evts_good_mean - 5 * u[:,i], 'blue')
    plt.axis('off')
    plt.title('PC' + str(i) + ': ' + str(round(s[i]/sum(s)*100)) +'%')

fname = 'figsSortingDIY/explore-evts-PC0to3.png'
plt.savefig(fname)
plt.close()
fname

for i in range(4,8):
    plt.subplot(2,2,i-3)
    plt.plot(evt_idx,evts_good_mean, 'black',
             evt_idx,evts_good_mean + 5 * u[:,i], 'red',
             evt_idx,evts_good_mean - 5 * u[:,i], 'blue')
    plt.axis('off')
    plt.title('PC' + str(i) + ': ' + str(round(s[i]/sum(s)*100)) +'%')

plt.savefig('figsSortingDIY/explore-evts-PC4to7.png')
plt.close()
'figsSortingDIY/explore-evts-PC4to7.png'

evts_good_P0_to_P3 = np.dot(evts[goodEvts,:],u[:,0:4])
swp.splom(evts_good_P0_to_P3.T,
          ['PC 0','PC 1','PC 2','PC 3'],
          marker='.',linestyle='None',
          alpha=0.2,ms=1)

fname = 'figsSortingDIY/Fig4.png'
plt.savefig(fname)
plt.close()
fname

evts_good_P4_to_P7 = np.dot(evts[goodEvts,:],u[:,4:8])
swp.splom(evts_good_P4_to_P7.transpose(),
          ['PC 4','PC 5','PC 6','PC 7'],
          marker='.',linestyle='None',
          alpha=0.2,ms=1)

fname = 'figsSortingDIY/Fig5.png'
plt.savefig(fname)
plt.close()
fname

import csv
f = open('evts.csv','w')
w = csv.writer(f)
w.writerows(np.dot(evts[goodEvts,:],u[:,:8]))
f.close()

from scipy.cluster.vq import kmeans2
centroid_10, c10 = kmeans2(np.dot(evts[goodEvts,:],u[:,0:3]),
                           k=10, minit='++', seed=20110928)

cluster_median = list([(i,
                        np.apply_along_axis(np.median,0,
                                            evts[goodEvts,:][c10 == i,:]))
                                            for i in range(10)
                                            if sum(c10 == i) > 0])
cluster_size = list([np.sum(np.abs(x[1])) for x in cluster_median])
new_order = list(reversed(np.argsort(cluster_size)))
new_order_reverse = sorted(range(len(new_order)), key=new_order.__getitem__)
c10b = [new_order_reverse[i] for i in c10]

centroid_9, c9 = kmeans2(np.dot(evts[goodEvts,:],u[:,0:3]),
                         k=9, minit='++', seed=20110928)
cluster_median9 = list([(i,
                         np.apply_along_axis(np.median,0,
                                             evts[goodEvts,:][c9 == i,:]))
                        for i in range(9)
                        if sum(c9 == i) > 0])
cluster_size9 = list([np.sum(np.abs(x[1])) for x in cluster_median9])
new_order9 = list(reversed(np.argsort(cluster_size9)))
new_order_reverse9 = sorted(range(len(new_order9)), key=new_order9.__getitem__)
c9b = [new_order_reverse9[i] for i in c9]

centroid_11, c11 = kmeans2(np.dot(evts[goodEvts,:],u[:,0:3]),
                         k=11, minit='++', seed=20110928)
cluster_median11 = list([(i,
                         np.apply_along_axis(np.median,0,
                                             evts[goodEvts,:][c11 == i,:]))
                        for i in range(11)
                        if sum(c11 == i) > 0])
cluster_size11 = list([np.sum(np.abs(x[1])) for x in cluster_median11])
new_order11 = list(reversed(np.argsort(cluster_size11)))
new_order_reverse11 = sorted(range(len(new_order11)), key=new_order11.__getitem__)
c11b = [new_order_reverse11[i] for i in c11]

plt.subplot(511)
swp.plot_events(evts[goodEvts,:][np.array(c10b) == 0,:])
plt.ylim([-22,16])
plt.subplot(512)
swp.plot_events(evts[goodEvts,:][np.array(c10b) == 1,:])
plt.ylim([-22,16])
plt.subplot(513)
swp.plot_events(evts[goodEvts,:][np.array(c10b) == 2,:])
plt.ylim([-22,16])
plt.subplot(514)
swp.plot_events(evts[goodEvts,:][np.array(c10b) == 3,:])
plt.ylim([-22,16])
plt.subplot(515)
swp.plot_events(evts[goodEvts,:][np.array(c10b) == 4,:])
plt.ylim([-22,16])

fname = 'figsSortingDIY/events-clusters0to4.png'
plt.savefig(fname)
plt.close()
fname

plt.subplot(511)
swp.plot_events(evts[goodEvts,:][np.array(c10b) == 5,:])
plt.ylim([-12,10])
plt.subplot(512)
swp.plot_events(evts[goodEvts,:][np.array(c10b) == 6,:])
plt.ylim([-12,10])
plt.subplot(513)
swp.plot_events(evts[goodEvts,:][np.array(c10b) == 7,:])
plt.ylim([-12,10])
plt.subplot(514)
swp.plot_events(evts[goodEvts,:][np.array(c10b) == 8,:])
plt.ylim([-12,10])
plt.subplot(515)
swp.plot_events(evts[goodEvts,:][np.array(c10b) == 9,:])
plt.ylim([-12,10])

fname = 'figsSortingDIY/events-clusters5to9.png'
plt.savefig(fname)
plt.close()
fname

f = open('evtsSorted.csv','w')
w = csv.writer(f)
w.writerows(np.concatenate((np.dot(evts[goodEvts,:],u[:,:8]),
                            np.array([c10b]).T),
                            axis=1))
f.close()

def filtered_reference(cluster_index,
                       labels_lst,
                       spike_pos,
                       data,
                       filter_length,
                       before,
                       after):
    """Computes the median of the events whose label is 'cluster_index'
    on a filtered and MAD normalized version of 'data'.

    Parameters
    ----------
    cluster_index: an integer between 0 and the number of clusters - 1
    labels_list: a list of labels containing the cluster to which events
                 have been attributed
    spike_pos: an array of spike indices (same length as labels_list)
    data: the data
    filter_length: a positive integer
    before: positive integer, the number of points before the 'peak' in
            the window (see mk_events)
    after: positive integer, the number of points after the 'peak' in
            the window (see mk_events)

    Returns
    -------
    An array with the 'reference' waveform.
    """
    data_filtered = [d for d in data]
    data_filtered = apply(lambda x:
                      fftconvolve(x,np.ones(filter_length)/filter_length,'same'),
                      1,np.array(data_filtered))
    dfiltered_mad = apply(swp.mad,1,data_filtered)
    data_filtered = (data_filtered.transpose() / \
                 dfiltered_mad_original).transpose()
    sp = spike_pos[[labels_lst[i]==cluster_index for i in range(len(labels_lst))]]
    evts = swp.mk_events(sp,data_filtered,before,after)
    res = np.apply_along_axis(np.median,0,evts)
    return res

plt.subplot(5,2,1)
ref = filtered_reference(0,c10b,sp0[goodEvts],data,5,14,30)
ref_min = np.floor(min(ref))
ref_max = np.ceil(max(ref))
plt.plot(ref,color='black',lw=1)
plt.axhline(y=-4,color="red",linestyle="dashed")
plt.axhline(y=-7,color="blue",linestyle="dotted")
plt.ylim([ref_min,ref_max])
plt.axis('off')
plt.title('Cluster '+str(0))
for i in range(1,10):
    plt.subplot(5,2,i+1)
    ref = filtered_reference(i,c10b,sp0[goodEvts],data,5,14,30)
    plt.plot(ref,color='black',lw=1)
    plt.axhline(y=-4,color="red",linestyle="dashed")
    plt.axhline(y=-7,color="blue",linestyle="dotted")
    plt.ylim([ref_min,ref_max])
    plt.axis('off')
    plt.title('Cluster '+str(i))

fname = 'figsSortingDIY/filtered-reference.png'
plt.savefig(fname)
plt.close()
fname

cluster_median_o = [cluster_median[new_order[i]][1] for i in range(10)]

cluster_median_n = [[cluster_median_o[i][45*j:45*(j+1)]/\
                     abs(min(cluster_median_o[i][45*j:45*(j+1)]))
                     for j in range(4)] for i in range(10)]

cluster_median_mean = np.zeros(45)
for i in range(7):
    for j in range(4):
        cluster_median_mean += cluster_median_n[i][j]
cluster_median_mean /= 28

for i in range(7):
    plt.subplot(3,3,i+1)
    plt.plot(cluster_median_mean,color='red',lw=2)
    plt.plot(np.array(cluster_median_n[i]).transpose(),color='black')
    plt.ylim([-1.1,0.9])
    plt.axis('off')
    plt.title('Cluster '+str(i))

fname = 'figsSortingDIY/fig-shape-comparison.png'
plt.savefig(fname)
plt.close()
fname

c3_median = apply(np.median,0,evts[goodEvts,:][np.array(c10b)==3,:])

plt.plot(c3_median[:],color='red')
plt.plot(evts[goodEvts,:][np.array(c10b)==3,:][13,:],color='black')

fname = 'figsSortingDIY/JitterIllustrationCluster3Event13.png'
plt.savefig(fname)
plt.close()
fname

dataD = apply(lambda x: fftconvolve(x,np.array([1,0,-1])/2.,'same'),
              1, data)
evtsED = swp.mk_events(sp0,dataD,14,30)
dataDD = apply(lambda x: fftconvolve(x,np.array([1,0,-1])/2.,'same'),
               1, dataD)
evtsEDD = swp.mk_events(sp0,dataDD,14,30)
c3D_median = apply(np.median,0,
                   evtsED[goodEvts,:][np.array(c10b)==3,:])
c3DD_median = apply(np.median,0,
                    evtsEDD[goodEvts,:][np.array(c10b)==3,:])

plt.plot(evts[goodEvts,:][np.array(c10b)==3,:][13,:]-\
         c3_median,color='black',lw=2)
plt.plot(1.3*c3D_median,color='red',lw=2)

fname = 'figsSortingDIY/JitterIllustrationCluster3Event13b.png'
plt.savefig(fname)
plt.close()
fname

delta_hat = np.dot(c3D_median,
                   evts[goodEvts,:][np.array(c10b)==3,:][13,:]-\
                   c3_median)/np.dot(c3D_median,c3D_median)
delta_hat

def rss_fct(delta,evt,center,centerD,centerDD):
    return np.sum((evt - center - delta*centerD - delta**2/2*centerDD)**2)

urss_fct = np.frompyfunc(lambda x:
                         rss_fct(x,
                                 evts[goodEvts,:]\
                                 [np.array(c10b)==3,:][13,:],
                                 c3_median,c3D_median,c3DD_median),1,1)

plt.subplot(1,2,1)
dd = np.arange(-5,5,0.05)
plt.plot(dd,urss_fct(dd),color='black',lw=2)
plt.subplot(1,2,2)
dd_fine = np.linspace(delta_hat-0.5,delta_hat+0.5,501)
plt.plot(dd_fine,urss_fct(dd_fine),color='black',lw=2)
plt.axvline(x=delta_hat,color='red')

fname = 'figsSortingDIY/JitterIllustrationCluster3Event13c.png'
plt.savefig(fname)
plt.close()
fname

def rssD_fct(delta,evt,center,centerD,centerDD):
    h = evt - center
    return -2*np.dot(h,centerD) + \
      2*delta*(np.dot(centerD,centerD) - np.dot(h,centerDD)) + \
      3*delta**2*np.dot(centerD,centerDD) + \
      delta**3*np.dot(centerDD,centerDD)

def rssDD_fct(delta,evt,center,centerD,centerDD):
    h = evt - center
    return 2*(np.dot(centerD,centerD) - np.dot(h,centerDD)) + \
      6*delta*np.dot(centerD,centerDD) + \
      3*delta**2*np.dot(centerDD,centerDD)

rss_at_delta0 = rss_fct(delta_hat,
                        evts[goodEvts,:][np.array(c10b)==3,:][13,:],
                        c3_median,c3D_median,c3DD_median)
rssD_at_delta0 = rssD_fct(delta_hat,
                          evts[goodEvts,:][np.array(c10b)==3,:][13,:],
                          c3_median,c3D_median,c3DD_median)
rssDD_at_delta0 = rssDD_fct(delta_hat,
                            evts[goodEvts,:][np.array(c10b)==3,:]\
                            [13,:],c3_median,c3D_median,c3DD_median)
delta_1 = delta_hat - rssD_at_delta0/rssDD_at_delta0

plt.plot(dd_fine,urss_fct(dd_fine),color='black',lw=2)
plt.axvline(x=delta_hat,color='red')
plt.plot(dd_fine,
         rss_at_delta0 + (dd_fine-delta_hat)*rssD_at_delta0 + \
         (dd_fine-delta_hat)**2/2*rssDD_at_delta0,color='blue',lw=2)
plt.axvline(x=delta_1,color='grey')

fname = 'figsSortingDIY/JitterIllustrationCluster3Event13d.png'
plt.savefig(fname)
plt.close()
fname

plt.plot(evts[goodEvts,:][np.array(c10b)==3,:][13,:]-\
         c3_median-delta_1*c3D_median-delta_1**2/2*c3DD_median,
         color='red',lw=2)
plt.plot(evts[goodEvts,:][np.array(c10b)==3,:][13,:],
         color='black',lw=2)
plt.plot(c3_median+delta_1*c3D_median+delta_1**2/2*c3DD_median,
         color='blue',lw=1)

fname ='figsSortingDIY/JitterIllustrationCluster3Event13e.png'
plt.savefig(fname)
plt.close()
fname

centers = { "Cluster " + str(i) :
            swp.mk_center_dictionary(sp0[goodEvts][np.array(c10b)==i],
                                     np.array(data),
                                     before=49, after=80)
            for i in range(10)}

long_motif_range = [min(centers['Cluster 0']['center']),
                    max(centers['Cluster 0']['center'])]
for i in range(5):
    plt.subplot(5,1,i+1)
    plt.plot(centers['Cluster '+str(i)]['center'],color='blue')
    plt.plot(centers['Cluster '+str(i)]['centerD'],color='orange')
    plt.ylim(long_motif_range)
    plt.axis('off')
    plt.title('Cluster ' + str(i))

fname = 'figsSortingDIY/first-5-long-motifs.png'
plt.savefig(fname)
plt.close()
fname

for i in range(5,10):
    plt.subplot(5,1,i-4)
    plt.plot(centers['Cluster '+str(i)]['center'],color='blue')
    plt.plot(centers['Cluster '+str(i)]['centerD'],color='orange')
    plt.ylim(long_motif_range)
    plt.axis('off')
    plt.title('Cluster ' + str(i))

fname = 'figsSortingDIY/last-5-long-motifs.png'
plt.savefig(fname)
plt.close()
fname

data0 = np.array(data) 
round0 = [swp.classify_and_align_evt(sp0[i],data0,centers)
          for i in range(len(sp0))]

nb_total = 0
print('Number of events attributed to each cluster / neuron')
print('----------------------------------------------------')
for i in range(10):
    nb = len([x[1] for x in round0 if x[0] == 'Cluster '+str(i)])
    nb_total += nb
    msg = 'Cluster {0:3}: {1:5}'.format(i,nb)
    print(msg)
nb = len([x[1] for x in round0 if x[0] == '?'])
nb_total += nb
msg = '          ?: {0:5}'.format(nb)
print(msg)
msg = 'Total      : {0:5}'.format(nb_total)
print(msg)
print('----------------------------------------------------')

pred0 = swp.predict_data(round0,centers,data_length=data0.shape[1])

data1 = data0 - pred0

plt.plot(tt, data0[0,], color='black',lw=0.5)
plt.plot(tt, data1[0,], color='red',lw=0.3)
plt.plot(tt, data0[1,]-20, color='black',lw=0.5)
plt.plot(tt, data1[1,]-20, color='red',lw=0.3)
plt.plot(tt, data0[2,]-40, color='black',lw=0.5)
plt.plot(tt, data1[2,]-40, color='red',lw=0.3)
plt.plot(tt, data0[3,]-60, color='black',lw=0.5)
plt.plot(tt, data1[3,]-60, color='red',lw=0.3)
plt.xlabel('Time (s)')
plt.xlim([1.8,1.9])
plt.ylim([-70,10])
plt.axis('off')

fname = 'figsSortingDIY/FirstPeeling.png'
plt.savefig(fname)
plt.close()
fname

data_filtered = -data1
data_filtered = apply(lambda x:
                      fftconvolve(x,np.array([1,1,1,1,1])/5.,'same'),
                      1,np.array(data_filtered))
data_filtered = (data_filtered.transpose() / \
                 dfiltered_mad_original).transpose()
data_filtered[data_filtered < 4] = 0
sp1 = swp.peak(data_filtered[0,:])

round1 = [swp.classify_and_align_evt(sp1[i],data1,centers)
          for i in range(len(sp1))]
pred1 = swp.predict_data(round1,centers,data_length=data1.shape[1])
data2 = data1 - pred1

nb_total = 0
print('Number of events attributed to each cluster / neuron')
print('----------------------------------------------------')
for i in range(10):
    nb = len([x[1] for x in round1 if x[0] == 'Cluster '+str(i)])
    nb_total += nb
    msg = 'Cluster {0:3}: {1:5}'.format(i,nb)
    print(msg)
nb = len([x[1] for x in round1 if x[0] == '?'])
nb_total += nb
msg = '          ?: {0:5}'.format(nb)
print(msg)
msg = 'Total      : {0:5}'.format(nb_total)
print(msg)
print('----------------------------------------------------')

plt.plot(tt, data1[0,], color='black',lw=0.5)
plt.plot(tt, data2[0,], color='red',lw=0.3)
plt.plot(tt, data1[1,]-20, color='black',lw=0.5)
plt.plot(tt, data2[1,]-20, color='red',lw=0.3)
plt.plot(tt, data1[2,]-40, color='black',lw=0.5)
plt.plot(tt, data2[2,]-40, color='red',lw=0.3)
plt.plot(tt, data1[3,]-60, color='black',lw=0.5)
plt.plot(tt, data2[3,]-60, color='red',lw=0.3)
plt.xlabel('Time (s)')
plt.xlim([1.8,1.9])
plt.ylim([-70,10])
plt.axis('off')

fname = 'figsSortingDIY/SecondPeeling.png'
plt.savefig(fname)
plt.close()
fname

nc_r1_pos = [x[1] for x in round1 if x[0] == '?']
nc_r1_evts = swp.mk_events(nc_r1_pos,data1,14,30)
swp.plot_events(nc_r1_evts,show_median=False,show_mad=False,events_lw=0.5)

fname = 'figsSortingDIY/non-classified-round1.png'
plt.savefig(fname)
plt.close()
fname

trains = [sorted([x[1]/15.0 for x in round0 if x[0] == 'Cluster '+str(i)]+
           [x[1]/15.0 for x in round1 if x[0] == 'Cluster '+str(i)])
          for i in range(10)]

isi_lst = [[st[i+1]-st[i] for i in range(len(st)-1)] for st in trains]
np.set_printoptions(precision=1)
[mquantiles(x,prob=[0,0.25,0.5,0.75,1]) for x in isi_lst]
