(setq org-export-html-postamble nil)
(require 'ox-publish)
(setq org-publish-project-alist
      '(
	("web-notes"
	 :base-directory "~/gitlab/spike-sorting-the-diy-way/org/"
	 :base-extension "org"
	 :publishing-directory "~/gitlab/spike-sorting-the-diy-way/public/"
	 :exclude "~"
	 :recursive t
	 :publishing-function org-html-publish-to-html
	 :headline-levels 4             ; Just the default for this project.
	 :auto-preamble t
	 )
	("web-static"
	 :base-directory "~/gitlab/spike-sorting-the-diy-way/org/"
	 :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf\\|bib\\|code\\|data\\|Rnw\\|org\\|dat\\|gz\\|RData\\|odt\\|txt\\|R\\|py\\|html\\|hdf5\\|npz\\|ipynb"
	 :publishing-directory "~/gitlab/spike-sorting-the-diy-way/public/"
	 :exclude "~"
	 :recursive t
	 :publishing-function org-publish-attachment
	 )
	("web" :components ("web-notes" "web-static"))))
(setq org-export-babel-evaluate nil)
