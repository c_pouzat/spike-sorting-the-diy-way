# Spike sorting the 'Do It Yourself' way

This is the repository associated with the page [Spike Sorting the 'Do It Yourself' way](https://c_pouzat.gitlab.io/spike-sorting-the-diy-way/).

It contains the source code of a course / tutorial on spike sorting given at the [Advanced tools for data analysis in Neuroscience - 2022](https://www.neurex.org/events/events-to-come/item/556-summer-school-advanced-tools-for-data-analysis-in-neuroscience-2022) summer school in Strasbourg, September 5-10.

A `Python` script reproducing, with all the figures, the analysis presented in the document _Spike Sorting the 'Do It Yourself' way_ is located in the `org/code` folder under the name `sorting_example.py`. Just download it and run it, all the necessary stuff, data and custom developped codes, will be downloaded first before performing the analysis. It takes less than a minute to run on my (old) laptop.
